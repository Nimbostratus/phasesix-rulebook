A character is created by summarising the character's career in templates. These templates determine the character's abilities and characteristics. There are no pre-made character classes or profiles.

### Lineage and career points

A Phase Six character always has a lineage. For all humans (and therefore most adventures) this will be the 'human' lineage, but in other settings it may also be elves, dwarves, androids or robots. 

Lineage determines the starting values of some aspects of a character. It can also affect skills and attributes, and has its own rules.

#### Base Game

* Career points (20)
* Actions (2)
* Persona and physis traits (1)
* Minimum roll (5+)
* Bonus, destiny and rerolls (0)
* Basic protection (0)
* Evasion (0)
* Maximum Health (6)

#### Magic Extension

* Arcana (0)
* Spell points (0)

#### Horror Extension

* Maximum stress (8)

The values in brackets are the starting values of the character. Career points can be used to select and combine templates when creating a character, which will make up the character's career and therefore all of their values. These templates can be chosen from any of the categories. Together they make up the character's skills, attributes, shadows and knowledge.

### The lineage template

Each lineage has its own character template, which can be added to a character's career at no point cost. A lineage template can only be selected if the character has that lineage.

This template has the following properties for humans:

* Rerolls: 2
* Bonus dice: 2

### Extensions and eras

At the start of a Phase Six campaign or adventure, the GM chooses the era in which the adventure will take place. Phase Six offers a choice of several eras.

* Classical Antiquity
* Middle Ages, Vikings and Crusades
* The Victorian Era and the Wild West
* Imperialism and World Wars
* The Cold War and the 80s
* Modern Times
* Science Fiction

In addition to the templates, the eras also determine the equipment that the characters can receive. The Middle Ages, Vikings and Crusades can of course also be used for a fantasy setting that does not take place on Earth.

There are also optional expansions that the GM can choose for a scenario or adventure. These add special rules, items and templates to the game. Special expansions are:

* Magic
* Horror
* Pantheon

### Worlds

It is possible to create your own game world based on the expansions. This means that you can play in a specific time period and also choose the mechanics of the Horror expansion. 

In addition to these options, there are **worlds**, which combine several expansions, but are also an expansion themselves. A world also has its own items, enemies, spells, etc.

The world *Realms of Tirakan* combines the expansions ‘Middle Ages, Vikings and Crusades’, ‘Magic’ and ‘Pantheon’ and offers its own world with 1000 years of history and a multitude of peoples and stories.

In the world of *NEXUS*, players take on the role of agents of the NEXUS, a secret organisation that protects humanity from extraterrestrial and paranormal events. The adventures of the NEXUS take place in Modern Times, with the Horror expansion enabled.

If you want to play on Earth with flexible settings, you can choose the world *Terra*. There are no predefined settings here, everything is freely selectable.

### Character templates

A template is a specific stage in a character's life. Each template is assigned to one of *Education*, *Occupation*, *Talent*, *Interests*, *Character* or *Environment*. 

The Lineage Template is a special template. It is determined by your chosen lineage and does not cost any career points. This means that a human will always receive the 'human' template, which gives the character two bonus dice and two re-rolls.

Each template changes a small number of the character's attributes and skills for better or worse, and can bring knowledge or shadows with it. Templates can also contain their own rules, which the character then adopts. For example, the *Blood Magic* template from the Magic expansion includes the rule to use wounds instead of arcana to cast spells. 

Each template is worth a certain number of career points. This is the number of points you must spend to add the template to your career. 

### Assemble templates

To create a character, templates from the chosen era and any expansions are selected until no career points are left or the player is satisfied with the character. These templates can be chosen from any category. It is also possible to select multiple occupations, i.e. all occupations that the character has ever performed or currently performs. 

The attributes, skills, shadows and knowledge of the templates are summed up and added to the starting value of the lineage on the character sheet. All values can be negative (see [[chapter-rolls-and-checks|Rolls and Checks]]).

### Completing the character

If you are satisfied with the composition of the templates, you can simply declare the character *finished*. If there are any development points left and not spent, they will be added to the character's *reputation* (see [[chapter-advancement|Advancement]]). So no points are lost.

#### Gear and money

Once character stats have been determined using the templates, the character can be equipped. The GM sets a starting capital for the characters for the campaign or adventure.

This starting capital can be used to purchase equipment such as weapons, armour and items. See the [[chapter-gear|Gear]] chapter for more details.

#### Magic expansion: Spells

If the magic expansion is used in the adventure or campaign, the character can also learn spells. This is done using 'spell points', which are determined by the templates. More details can be found in the [[chapter-magic|Magic]].
