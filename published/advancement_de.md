Im Laufe der Zeit sammelt der Charakter Erfahrung und entwickelt sich weiter. Die Weiterentwicklung erfolgt auf Basis von Schablonen.

### Reputation

Die Reputation eines Charakters gibt an, wie bekannt er ist und wie viel Erfahrung er im Leben gesammelt hat.

Die Reputation wird in verbrauchte und gesammelte Reputation unterteilt, die durch einen Schrägstrich getrennt sind. Die erhaltenen Reputationspunkte stehen hinter dem Schrägstrich, die für den Aufstieg verwendeten Reputationspunkte davor. Für nicht verbrauchte Reputationspunkte können Charakterschablonen erworben werden.

#### Reputation erlangen

Der Charakter erhält Reputation für bestandene Abenteuer. Die Reputation wird vom Spielleiter vergeben und sollte zwischen 5 und 10 pro Sitzung liegen. 

Reputation kann auch direkt für einzelne Aktionen vergeben werden. Eine gelungene Aktion oder Szene im Spiel kann dazu führen, dass der Spielleiter eine bestimmte Anzahl an Reputationspunkten vergibt.


### Den Charakter weiterentwickeln

Der Spieler kann nach jeder Spielsitzung Schablonen aus den Bereichen *Charakter*, *Interessen*, *Lebensumstände* und *Talent* erwerben. Neue Schablonen aus den Bereichen *Beruf* und *Bildung* können nicht hinzugefügt werden.

Die Weiterentwicklung findet jeweils nach einer Spielsitzung statt, so dass der Charakter auch während eines Abenteuers um weitere Schablonen erweitert werden kann. Die Schablonen werden wie bei der Charaktererschaffung eingetragen. Änderungen der Werte werden direkt auf dem Charakterbogen verrechnet.
