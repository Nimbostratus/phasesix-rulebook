Ein Charakter wird erstellt, indem der Werdegang des Charakters in Schablonen zusammengefasst wird. Diese Schablonen bestimmen die Fähigkeiten und Eigenschaften des Charakters. Es gibt keine vorgefertigten Charakterklassen oder -profile.

### Abstammung und Werdegangspunkte

Ein Phase Six Charakter hat immer eine Abstammung. Für alle Menschen (und damit für die meisten Abenteuer) ist dies die Abstammung "Mensch", aber in anderen Settings kann es auch Elfen, Zwerge, Androiden oder Roboter geben. 

Die Abstammung bestimmt die Startwerte einiger Aspekte des Charakters. Sie kann aber auch Fertigkeiten und Attribute beeinflussen und eigene Regeln mit sich bringen.

#### Basisspiel

* Werdegangspunkte (20)
* Aktionen (2)
* Persona- und Physiseigenschaften (1)
* Mindestwurf (5+)
* Bonus-, Schicksals- und Wiederholungswürfe (0)
* Basisschutz (0)
* Ausweichen (0)
* Maximale Lebenspunkte (10)

#### Magie Erweiterung

* Arkana (0)
* Zauberpunkte (0)

#### Horror Erweiterung

* Maximaler Stress (8)

Die Werte in Klammern geben die Startwerte des Menschen an. Mit Hilfe der Werdegangspunkte können bei der Charaktererstellung Schablonen ausgewählt und zusammengestellt werden, die den Werdegang und damit alle Werte des Charakters ausmachen. Diese Schablonen können aus allen Kategorien zusammengestellt werden. Zusammen ergeben sie die Fähigkeiten, Attribute, Schatten und das Wissen des Charakters.

### Die Abstammungsschablone

Jeder Abstammung ist eine eigene Charakterschablone zugeordnet, die ohne Punktekosten zum Werdegang des Charakters hinzugefügt werden kann. Die jeweilige Abstammungsschablone kann nur gewählt werden, wenn der Charakter die entsprechende Abstammung besitzt.

Für den Menschen bringt diese Schablone folgende Eigenschaften mit sich:

* Wiederholungswürfe: 2
* Bonuswürfel: 2

### Erweiterungen und Epochen

Zu Beginn einer Phase-Six-Kampagne oder eines Abenteuers legt der Spielleiter die Epoche fest, in der das Abenteuer spielt. Phase Six bietet verschiedene Erdzeitalter zur Auswahl.

* Die klassische Antike
* Mittelalter, Wikinger und Kreuzzüge
* Viktorianisches Zeitalter und der Wilde Westen
* Imperialismus und Weltkriege
* Der kalte Krieg und die 80er
* Moderne Zeit
* Science Fiction

Die Epochen bestimmen neben den Schablonen auch die Ausrüstung, die die Charaktere erhalten können. Mittelalter, Wikinger und Kreuzzüge können so natürlich auch für ein Fantasy-Setting verwendet werden, das nicht auf der Erde spielt.

Darüber hinaus gibt es optionale Erweiterungen, die vom Spielleiter für ein Szenario oder ein Abenteuer ausgewählt werden können. Diese fügen dem Spiel spezielle Regeln, Gegenstände und Schablonen hinzu. Spezielle Erweiterungen sind:

* Magie
* Horror
* Pantheon

### Welten

Auf der Basis der Erweiterungen ist es möglich, eine eigene Spielwelt zu erschaffen. So kann man in einer bestimmten Erdzeit spielen und zusätzlich die Mechaniken der Horror-Erweiterung wählen. 

Zusätzlich zu diesen Möglichkeiten gibt es **Welten**, die mehrere Erweiterungen zusammenfassen, aber auch selbst eine Erweiterung darstellen. Eine Welt bringt also auch eigene Gegenstände, Gegner, Zaubersprüche usw. mit sich.

Die Welt *Tirakans Reiche* vereint die Erweiterungen "Mittelalter, Wikinger und Kreuzzüge", "Magie" und "Pantheon" und bietet eine eigene Welt mit 1000 Jahren Geschichte und einer Vielzahl von Völkern und Geschichten.

In der Welt des *NEXUS* schlüpfen die Spieler in die Rolle von Agenten des NEXUS, einer Geheimorganisation zum Schutz der Menschheit vor außerirdischen und paranormalen Ereignissen. Die Abenteuer des NEXUS finden in der "Modernen Zeit" statt, die Erweiterung "Horror" ist aktiviert.

Um auf der Erde mit flexiblen Einstellungen zu spielen, kann die Welt *Terra* gewählt werden. Hier sind keine Einstellungen vorgegeben, alles ist frei wählbar.

### Schablonen

Eine Schablone ist eine bestimmte Station im Leben des Charakters. Jede Schablone ist einem der Bereiche *Bildung*, *Beruf*, *Talent*, *Interessen*, *Charakter* oder *Lebensumstände* zugeordnet. 

Eine besondere Schablone ist die Abstammungsschablone. Sie ist durch dir gewählte Abstammung vorgegeben und kostet keine Werdegangspunkte. So erhält ein Mensch immer die Schablone "Mensch", die dem Charakter zwei Bonuswürfel und zwei Wiederholungswürfe gibt.

Jede Schablone verändert eine kleine Anzahl von Eigenschaften und Fertigkeiten des Charakters zum Positiven oder Negativen und kann Wissen oder Schatten mit sich bringen. Außerdem können Schablonen eigene Regeln enthalten, die der Charakter dann übernimmt. Die Schablone *Blutmagie* aus der Magie-Erweiterung bringt z.B. die Regel auch Wunden statt Arkana zum Zaubern zu verwenden. 

Jede Schablone ist eine gewisse Zahl an Werdegangspunkten wert. Dies ist die Anzahl der Punkte, die man aufwenden muss, um die Schablone in den eigenen Werdegang zu übernehmen. 

### Schablonen zusammenstellen

Um einen Charakter zu erstellen, werden so lange Schablonen aus der gewählten Epoche und eventuellen Erweiterungen ausgewählt, bis keine Werdegangspunkte mehr übrig sind oder der Spieler mit dem Charakter zufrieden ist. Diese Schablonen können beliebig aus allen Kategorien zusammengestellt werden. So ist es auch möglich, mehrere Berufe zu wählen, d.h. alle Berufe, die der Charakter in seinem Werdegang einmal ausgeübt hat oder noch parallel ausübt. 

Die Attribute, Fertigkeiten, Schatten und das Wissen der Schablonen werden zusammengezählt und zum Startwert der Abstammung auf dem Charakterbogen addiert. Dabei können alle Werte auch negativ werden (siehe [[chapter-rolls-and-checks|Würfe und Proben]]).

### Den Charakter fertigstellen

Ist der Spieler mit der Zusammenstellung der Schablonen zufrieden, kann er den Charakter einfach als *fertig* deklarieren. Sollten noch nicht ausgegebene Karrierepunkte übrig sein, so werden diese dem *Ruf* des Charakters hinzugefügt (siehe [[chapter-advancement|Steigern]]). Es gehen also keine Punkte verloren.

#### Ausrüstung und Vermögen

Nachdem die Charakterwerte durch die Schablonen festgelegt wurden, kann der Charakter noch mit Ausrüstung ausgestattet werden. Der Spielleiter legt für die Kampagne oder das Abenteuer ein Startkapital für die Charaktere fest.

Dieses Startkapital kann dafür verwendet werden, um Ausrüstung, wie etwa Waffen, Rüstungen und Gegenstände zu kaufen. Näheres dazu findet sich im Kapitel [[chapter-gear|Ausrüstung]].

#### Magie-Erweiterung: Zauber

Wird im Abenteuer oder der Kampagne die Magie-Erweiterung verwendet, so kann der Charakter auch Zauber erlernen. Dazu werden "Zauberpunkte", welche durch die Schablonen festgelegt werden, verwendet. Näheres dazu findet sich im Kapitel [[chapter-magic|Magie]].