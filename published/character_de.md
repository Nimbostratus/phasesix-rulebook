Jeder Spieler erstellt einen Charakter, um ein Phase-Six-Abenteuer zu spielen. Der Charakter wird mit allen relevanten Werten auf dem Charakterbogen festgehalten. 

Im Kapitel [[chapter-create-a-character|Einen Charakter erstellen]] wird genau beschrieben, wie ein neuer Charakter erstellt wird. In diesem Teil werden die grundlegenden Eigenschaften beschrieben, die einen Charakter in Phase Six ausmachen.

### Persona

Die Werte unter Persona beziehen sich auf die mentalen Eigenschaften des Charakters. Jeder Wert entspricht einer Persönlichkeitseigenschaft. Die Persona-Eigenschaften haben jeweils einen eigenen Wert und bilden auch die Grundlage für die Fertigkeiten.

#### Bildung

Bildung beschreibt das angeeignete Allgemeinwissen des Charakters. Ein Charakter mit hoher Bildung ist gut in allen theoretischen Fertigkeiten wie etwa *Naturkunde* oder *Historie*.

#### Logik

Im Gegensatz zur *Bildung* bezieht sich der Wert Logik auf die Fähigkeit, vernünftige Schlussfolgerungen zu ziehen. Logik ist besonders wichtig, wenn es darum geht, Probleme durch logisches Denken zu lösen. Ein Charakter mit hoher Logik ist besser in Fertigkeiten wie *Untersuchung* oder *Mechanik*.

#### Gewissenhaftigkeit

Wie gewissenhaft geht der Charakter an Aufgaben oder Tätigkeiten heran? Eine geringe Gewissenhaftigkeit führt zu Nachlässigkeit, während eine hohe Gewissenhaftigkeit ein organisiertes und effektives Vorgehen sicherstellt.

#### Willenskraft

Willenskraft bezieht sich auf die Fähigkeit des Charakters, seine eigenen Vorstellungen und Prinzipien durchzusetzen. Ein willensstarker Charakter ist unter anderem gut in den Fertigkeiten *Tapferkeit* und *Einschüchtern*.

#### Auffassungsgabe

Die Auffassungsgabe beschreibt die Fähigkeit des Charakters, seine Umgebung wahrzunehmen und Informationen aufzunehmen. Ein Charakter mit guter Auffassungsgabe ist gut in den Fertigkeiten *Wahrnehmung* und *Orientierung*.

#### Charme

Eine Person mit hohem Charme versteht es, auf andere Menschen zuzugehen und eine positive Wirkung auf sie auszuüben. Dieser Wert ist nicht zu verwechseln mit der körperlichen Eigenschaft *Attraktivität*. Ein charmanter Charakter ist unter anderem gut in *Politik* und *Empathie*.

### Physis

Alle Physis-Attribute beschreiben die körperlichen Fähigkeiten des Charakters. Jedes Attribut hat einen Wert, der die Anzahl der Würfel angibt, die für dieses Attribut geworfen werden.

#### Geschick

Dieser Wert beschreibt sowohl die körperliche Geschicklichkeit als auch die Fingerfertigkeit des Charakters. Ein geschickter Charakter ist u.a. gut in *Darbietung* und *Heimlichkeit*.

#### Kraft

Kraft ist die reine Stärke des Charakters und kommt immer dann zum Einsatz, wenn es darum geht, etwas mit reiner Muskelkraft zu bewegen. Ein Charakter mit hoher Kraft kann gut *Werfen* und ist gut in *Athletik*.

#### Attraktivität

Attraktivität beschreibt die Ausstrahlung des Charakters. Dabei muss ein hoher Wert nicht unbedingt Schönheit bedeuten, auch ein markanter Charakter kann attraktiv sein.

#### Ausdauer

Ausdauer ist die körperliche Ausdauer des Charakters, also nicht im Sinne von Geduld (das ist eher *Gewissenhaftigkeit* unter Persona). Dieser Wert kommt also bei einem Dauerlauf oder einer lang andauernden, anstrengenden Tätigkeit zum Tragen.

#### Resistenz

Dieser Wert umfasst sowohl die Fähigkeit, Verletzungen oder Schmerzen zu ertragen oder zu vermeiden, als auch die Widerstandsfähigkeit gegen Krankheiten, Gifte oder Umwelteinflüsse wie Hitze oder Kälte.

#### Schnelligkeit

Schnelligkeit ist sowohl die Geschwindigkeit, mit der der Charakter sich bewegen kann, als auch die Fähigkeit schnell auf etwas zu reagieren.

### Der Mindestwurf

Der Mindestwurf ist ein zentrales Merkmal des Charakters. Er gibt an, welches Ergebnis ein Würfel haben muss, um einen Erfolg darzustellen. Der Mindestwurf wird von der Abstammung übernommen und liegt bei den meisten Charakteren bei 5+. Die Schablone "Meisterliche Präsenz" senkt den Mindestwurf um 1, ansonsten kann er nur durch besondere Ereignisse oder seltene Gegenstände und oft nur für kurze Zeit verändert werden.

### Ausweichen

*Ausweichen* wird im Kampf verwendet und erlaubt es, einem Nahkampfangriff auszuweichen. Der Wert ist gleich dem Ausweichwert der Abstammung plus dem Durchschnitt von Schnelligkeit und Geschicklichkeit (aufgerundet). Rüstung und Waffen reduzieren diesen Wert. Charakterschablonen können diesen Wert verändern.

### Schutz

Hat ein Charakter aufgrund seiner Abstammung Schutz, so gilt dieser als "angeborener Schutz". Diese Schutzeinheiten können benutzt werden, ohne dass sie sich erschöpfen, sie stehen bei jedem Angriff zur Verfügung.

### Zusatzwürfel

Jeder Charakter kann eine Anzahl von *Bonuswürfeln*, *Schicksalswürfeln* oder *Wiederholungswürfeln* haben. Alle drei haben unterschiedliche Anwendungen (siehe Würfe und Proben), stellen aber immer einen Vorteil für den Charakter dar, der während des Spiels genutzt werden kann. 

Der Charakter kann verbrauchte Würfel während der Rast zurückerhalten (siehe [[chapter-wounds-and-healing|Wunden und Heilung]]).

### Fertigkeiten

Komplexere Handlungen oder Kenntnisse werden durch *Fertigkeiten* beschrieben. Alle Charaktere haben die gleichen Fertigkeiten mit unterschiedlichen Werten, so dass der Spielleiter sicher sein kann, dass ein Spieler auf jeden Fall auf eine Fertigkeit würfeln kann. 

Jede Fertigkeit setzt sich aus einem Basiswert und einem Fertigkeitswert zusammen. Der Basiswert ist der Durchschnitt von zwei verschiedenen Attributen. So ist z.B. der Basiswert für die Fertigkeit *Einschüchtern* der Durchschnitt aus *Auffassungsgabe* und *Gewissenhaftigkeit*. 

Auf die Basisfertigkeit werden die Boni der gewählten Charakterschablonen addiert.

### Wissen

Wissen funktioniert ähnlich wie Fertigkeiten, allerdings ist hier die Liste nicht vorgegeben. Charaktere können aufgrund ihres Werdegangs verschiedene Wissensfertigkeiten haben, die sie frei einsetzen können. Wissen wird jeweils einer Fertigkeit zugeordnet. Der effektive Wert, auf den gewürfelt wird, setzt sich aus dem Wissenswert und dem Fertigkeitswert zusammen.

### Schatten

Ein Charakter kann besondere Eigenschaften haben, die ihn außerhalb seiner körperlichen oder psychischen Eigenschaften beeinflussen. Jeder *Schatten* hat seine eigene Beschreibung oder Regel. So kann ein Charakter z.B. einen Rivalen haben oder obrigkeitshörig sein. Schatten haben keine Werte, können aber ihre eigenen Regeln mitbringen.
