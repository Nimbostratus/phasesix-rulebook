The character gains experience over time and develops based on templates.

### Reputation

A character's reputation reflects their level of recognition and life experience.

Reputation is divided into spent and earned reputation, separated by a slash. Earned reputation points are placed after the slash, and used reputation points are placed before the slash. Unused reputation points can be used to purchase character templates.

#### Gaining reputation

The character gains reputation for successful quests. Reputation is awarded by the GM and should be between 5 and 10 per session. 

Reputation can also be awarded directly for individual actions. A successful action or scene in the game can result in the GM awarding a certain number of reputation points.

### Developing the character

After each game session, the player can obtain templates from the categories of *character*, *interests*, *environment*, and *talent*. It is not possible to add new templates from the *occupation* and *education* categories.

Further development takes place after each game session, so the character can be upgraded with additional templates during the course of an adventure. Templates are added in the same way as during character creation. Changes to values are applied directly to the character sheet.
