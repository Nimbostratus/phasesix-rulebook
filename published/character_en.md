Each player creates a character to play a Phase Six adventure. The character is recorded on the character sheet with all relevant values. 

The chapter [[chapter-create-a-character|Creating a character]] describes exactly how to create a new character. This section describes the basic characteristics that make up a character in Phase Six.

### Persona

The values listed under Persona refer to the mental characteristics of the character. Each value corresponds to a personality trait. Persona traits have their own value and are also the base for skills.

#### Education

Education describes the general knowledge a character has acquired. A character with a high level of education is good at all theoretical skills such as *Nature* or *History*.

#### Logic

Unlike *Education*, the value of logic relates to the ability to draw reasonable conclusions. Logic is especially important when it comes to solving problems through logical reasoning. A character with high Logic will be better at skills such as *Investigation* or *Mechanics*.

#### Conscientiousness

How conscientiously does the character approach tasks or activities? A low level of conscientiousness leads to carelessness, whereas a high level of conscientiousness ensures an organised and effective approach.

#### Willpower

Willpower refers to the character's ability to assert their own ideas and principles. A strong-willed character is good at the skills *Courage* and *Intimidation*, among others.

#### Apprehension

Perception describes a character's ability to perceive their surroundings and absorb information. A character with good perception is good at the skills *Perception* and *Orientation*.

#### Charm

A person with a high value for Charm knows how to approach other people and have a positive effect on them. This attribute is not to be confused with the physical trait of *Attractiveness*. A charming character is good at *Politics* and *Empathy*, among other things.

### Physis

All physical attributes describe the character's physical abilities. Each attribute has a value that indicates the number of dice rolled for that attribute.

#### Deftness

This stat describes both the physical dexterity and the agility of the character. A deft character is, among other things, good at *Performance* and *Stealth*.

#### Strength

Strength is the pure strength of the character and is used whenever it is necessary to move something with pure muscle power. A character with high Strength is good at *Throwing* and good at *Athletics*.

#### Attractiveness

Attractiveness describes the charisma of the character. A high value does not necessarily mean beauty; a distinctive character can also be attractive.

#### Endurance

Stamina is the physical endurance of the character, not in the sense of patience (that is more like *Conscientiousness* in Persona). This stat comes into play during an endurance run or a long, strenuous activity.

#### Resistance

This includes the ability to withstand or avoid injury or pain, as well as resistance to disease, toxins or environmental influences such as heat or cold.

#### Quickness

Speed is both the speed at which the character is able to move around and the ability to react quickly to a situation.

### The minimum roll

The minimum roll is a central feature of the character. It specifies the result a die must have to represent a success. The minimum roll is taken from the lineage and is 5+ for most characters. The "Masterly Presence" template lowers the minimum roll by 1, otherwise it can only be changed by special events or rare items, and often only for a short time.

### Evasion

*Evasion is used in combat and allows you to avoid a melee attack. It is equal to the Evasion value of the lineage plus the average of *Quickness* and *Deftness* (rounded up). Armour and weapons reduce this stat. Character templates can change this value.

### Protection

If a character has protection due to their lineage, this is called 'innate protection'. These protections can be used without depleting them; they are available for any attack.

### Additional dice

Each character can have a number of *bonus dice*, *destiny dice* or *rerolls*. All three have different uses (see Rolls and Checks), but always represent an advantage to the character that can be used during the game. 

The character can regain used dice during the rest (see [[chapter-wounds-and-healing|Wounds and healing]]).

### Skills

More complex actions or knowledge are described by *skills*. All characters have the same skills with different values, so the GM can be sure that a player can always roll a skill. 

Each skill has a base value and a skill value. The base value is the average of two different attributes. For example, the base value of the skill *Intimidate* is the average of *Apprehension* and *Conscientiousness*. 

The bonuses of the selected character templates are added to the basic skill.

### Knowledge

Knowledge works in a similar way to skills, but the list is not predefined. Characters can have different knowledge skills based on their background, which they can use freely. Knowledge is always associated with a skill. The effective die roll value is the sum of the knowledge value and the skill value.

### Shadows

A character can have special traits that affect them outside of their physical or mental attributes. Each *shadow* has its own description or rule. For example, a character may have a rival or be obedient to authority. Shadows do not have values, but can have their own rules.
